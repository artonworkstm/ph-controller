//import liraries
import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { bindActionCreators } from 'redux';
import { useDispatch, useSelector } from 'react-redux';

import { setFilter } from '../redux/actions/Payload';

// create a component
const DataFilter = ({ name }) => {
	const actions = bindActionCreators({ setFilter }, useDispatch());
	const { filterReducer: { filter } } = useSelector((state) => state);
	return (
		<View style={styles.container}>
			<TouchableOpacity
				onPress={() => actions.setFilter(name)}
			>
				<View style={[styles.border, { borderBottomColor: filter === name ? 'rgba(255,255,255,0.8)' : 'rgba(255,255,255,0.3)'  }]}>
					<Text style={[styles.text, { color: filter === name ? 'rgba(255,255,255,0.8)' : 'rgba(255,255,255,0.3)' }]}>{name}</Text>
				</View>
			</TouchableOpacity>
		</View>
	);
};

// define your styles
const styles = StyleSheet.create({
	container: {
		flex: 1,
		height: '15%',
		marginHorizontal: '3.5%',
		justifyContent: 'center',
		alignItems: 'center',
	},
	border: {
		height: '90%',
		paddingVertical: '20%',
		borderBottomWidth: 2,
	},
	text: {
		fontSize: 19,
		fontWeight: '600'
	}
});

//make this component available to the app
export default DataFilter;
