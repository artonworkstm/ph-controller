//import liraries
import React from 'react';
import { View, StyleSheet } from 'react-native';
import DataFilter from './DataFilter';
import { Chart } from './RealtimeDB';

// create a component
const DataVisualizer = () => {
	return (
		<View style={styles.container}>
			<View style={styles.chart}><Chart /></View>
			<View style={styles.filters}>
				<DataFilter name="minute" />
				<DataFilter name="hour" />
				<DataFilter name="day" />
				<DataFilter name="week" />
			</View>
		</View>
	);
};

// define your styles
const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'flex-end',
		alignItems: 'center',
		backgroundColor: '#2c3e50',
	},
	chart: {
		width: '100%',
		height: '85%',
	},
	filters: {
		width: '100%',
	  height: '100%',
	  display: 'flex',
	  flexDirection: 'row',
	  justifyContent: 'flex-end',
	  position: 'absolute',
	  top: '75%'
	}
});

//make this component available to the app
export default DataVisualizer;
