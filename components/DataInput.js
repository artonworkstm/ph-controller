//import liraries
import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { LinearGradient } from 'expo-linear-gradient';
import Licon from 'react-native-vector-icons/AntDesign';

import { SetPHValue, PhValue, Tanks, PhValueIndicator } from '../components/RealtimeDB';
import { logOut } from '../components/Authenticate';

// create a component
const DataInput = () => {
	return (
		<View style={styles.container}>
			<View
				style={{
					width: '100%',
					display: 'flex',
					justifyContent: 'center',
					alignItems: 'center',
					shadowColor: '#000',
					shadowOffset: {
						width: 0,
						height: 6,
					},
					shadowOpacity: 0.39,
					shadowRadius: 8.3,

					elevation: 13,
				}}
			>
				<LinearGradient
					style={{
						width: '100%',
						height: '100%',
						display: 'flex',
						justifyContent: 'center',
						alignItems: 'center',
					}}
					start={[0, 0.5]}
					end={[1, 0.5]}
					colors={['#8300FF', '#BE1EED']}
				>
					<View
						style={{
							width: '100%',
							height: '85%',
							display: 'flex',
							flexDirection: 'column',
							alignItems: 'center',
						}}
					>
						<View
							style={{
								width: '100%',
								height: '40%',
								display: 'flex',
								justifyContent: 'center',
								marginTop: '8%',
							}}
						>
							<View
								style={{
									display: 'flex',
									flexDirection: 'row',
									justifyContent: 'space-between',
								}}
							>
								<View style={{ width: '30%' }}></View>
								<Text
									style={{
										textAlign: 'center',
										color: 'rgba(255,255,255,0.8)',
										fontSize: 23,
										fontWeight: '600',
									}}
								>
                  Expected
								</Text>
								<View
									style={{
										width: '30%',
										display: 'flex',
										alignItems: 'center',
									}}
								>
									<TouchableOpacity onPress={() => logOut()}>
										<Licon
											name="logout"
											size={28}
											color="#fff"
											style={{ marginLeft: '40%' }}
										/>
									</TouchableOpacity>
								</View>
							</View>
							<View>
								<PhValue />
							</View>
						</View>
						<View
							style={{
								width: '100%',
								height: '60%',
								display: 'flex',
								flexDirection: 'column',
								alignItems: 'center',
							}}
						>
							<PhValueIndicator />
							<View
								style={{
									width: '110%',
									display: 'flex',
									flexDirection: 'row',
									justifyContent: 'space-around',
								}}
							>
								<Text style={styles.phIndicator}>5.2</Text>
								<Text style={styles.phIndicator}>6</Text>
								<Text style={styles.phIndicator}>6.8</Text>
							</View>
							<View style={{ width: '100%' }}>
								<View
									style={[styles.tank, { left: 0 }]}
								>
									<Tanks tank="ALKALIN" />
								</View>
								<SetPHValue />
								<View
									style={[styles.tank, { right: 0 }]}
								>
									<Tanks tank="ACID" />
								</View>
							</View>
						</View>
					</View>
				</LinearGradient>
			</View>
		</View>
	);
};

// define your styles
const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		backgroundColor: '#2c3e50',
	},
	phIndicator: {
		fontSize: 26,
		textAlign: 'center',
		fontWeight: '600',
		color: 'rgba(255, 255, 255, 0.4)',
		margin: '10%',
		marginBottom: '2%',
	},
	tank: {
		transform: [{ rotate: '270deg' }],
		position: 'absolute',
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'center',
		padding: '2%',
	}
});

//make this component available to the app
export default DataInput;
