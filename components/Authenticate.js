//import liraries
import React, { useState } from 'react';
import {
	View,
	Text,
	StyleSheet,
	TextInput,
	TouchableOpacity,
	Platform,
} from 'react-native';
import * as firebase from 'firebase/app';
import 'firebase/auth';
import { useFonts } from '@use-expo/font';
import { bindActionCreators } from 'redux';
import { useDispatch } from 'react-redux';

import { setError } from '../redux/actions/Payload';

export const logOut = () => {
	firebase.app().auth().signOut();
};

export const pwResetEmail = (email, action) => {
	firebase
		.app()
		.auth()
		.sendPasswordResetEmail(email)
		.then(() => setPwReset(false))
		.catch((err) => {
			switch (err.code) {
			case 'auth/user-not-found':
				return action('No such user found!');
			case 'auth/invalid-email':
				return action('Invalid e-mail address!');
			default:
				return action('Error');
			}
		});
};

/** auth/user-not-found */
/** auth/invalid-email */

// create a component
const Authenticate = () => {
	const actions = bindActionCreators({ setError }, useDispatch());
	const [fontsLoaded] = useFonts({
		Righteous: require('../assets/fonts/Righteous-Regular.ttf'),
	});
	const [auth, setAuth] = useState({
		username: null,
		psswd: null,
		isAuthed: false,
	});
	const { username, psswd, isAuthed } = auth;
	return (
		<View
			style={{
				width: '80%',
				height: '40%',
				backgroundColor: '#AC5CE5',
				borderRadius: 25,
				display: 'flex',
				justifyContent: 'center',
				alignItems: 'center',
				shadowColor: '#000',
				shadowOffset: {
					width: 0,
					height: 4,
				},
				shadowOpacity: 0.34,
				shadowRadius: 6.3,
				elevation: 12,
			}}
		>
			<View style={styles.input}>
				<TextInput
					onFocus={() => actions.setError(null)}
					autoCapitalize="none"
					style={{
						width: '100%',
						height: '100%',
						textAlign: 'center',
						color: '#fff',
						fontSize: 22,
					}}
					placeholderTextColor='rgba(255,255,255,0.6)'
					placeholder="username"
					value={username}
					onChange={(value) =>
						setAuth({
							username: value.nativeEvent.text,
							psswd,
							isAuthed,
						})
					}
				/>
			</View>
			<View style={styles.input}>
				<TextInput
					onFocus={() => actions.setError(null)}
					secureTextEntry={true}
					style={{
						width: '100%',
						height: '100%',
						textAlign: 'center',
						color: '#fff',
						fontSize: 22,
					}}
					placeholderTextColor='rgba(255,255,255,0.6)'
					placeholder="password"
					value={psswd}
					onChange={(value) =>
						setAuth({
							username,
							psswd: value.nativeEvent.text,
							isAuthed,
						})
					}
				/>
			</View>
			<View
				style={{
					width: '55%',
					height: '20%',
					display: 'flex',
					justifyContent: 'center',
					backgroundColor: '#8300A7',
					borderRadius: 32,
				}}
			>
				<TouchableOpacity
					onPress={async () => {
						(username && psswd) != null || ''
							? await firebase
								.app()
								.auth()
								.signInWithEmailAndPassword(username, psswd)
								.then(() => actions.setError(null))
								.catch((err) => {
									switch (err.code) {
									case 'auth/invalid-email':
										return actions.setError('Invalid e-mail address!');

									case 'auth/user-not-found':
										return actions.setError('Wrong e-mail or password');
									case 'auth/wrong-password':
										return actions.setError('Wrong e-mail or password');
									case 'auth/too-many-requests':
										return actions.setError('Please try again later!');
									default:
										return actions.setError(err.code);
									}
								})
							: actions.setError('Please fill everything!');
					}}
				>
					<Text
						style={{
							textAlign: 'center',
							fontFamily:
                Platform.OS === 'ios'
                	? fontsLoaded
                		? 'Righteous'
                		: 'Futura'
                	: fontsLoaded
                		? 'Righteous'
                		: 'sans-serif-medium',
							fontSize: 32,
							fontWeight: '600',
							color: '#fff',
						}}
					>
            Log in
					</Text>
				</TouchableOpacity>
			</View>
		</View>
	);
};

export const LogOut = () => {
	return (
		<View
			style={{
				width: '45%',
				height: '10%',
				display: 'flex',
				justifyContent: 'center',
				backgroundColor: '#8300A7',
				borderRadius: 32,
				marginBottom: 20,
			}}
		>
			<TouchableOpacity onPress={() => firebase.app().auth().signOut()}>
				<Text
					style={{
						textAlign: 'center',
						fontSize: 32,
						color: '#fff',
					}}
				>
          Log out
				</Text>
			</TouchableOpacity>
		</View>
	);
};

const styles = StyleSheet.create({
	input: {
		width: '85%',
		height: '20%',
		display: 'flex',
		alignItems: 'center',
		marginBottom: '10%',
		borderRadius: 25,
		backgroundColor: '#AC5CE5',
		borderColor: 'rgba(255,255,255, 0.2)',
		borderWidth: 2,
	},
});

//make this component available to the app
export default Authenticate;
