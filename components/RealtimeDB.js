//import liraries
import React, { PureComponent } from 'react';
import { View, Text, StyleSheet, Platform } from 'react-native';
import { Slider } from 'react-native-elements';
import { LinearGradient } from 'expo-linear-gradient';
import * as Haptics from 'expo-haptics';
import * as firebase from 'firebase/app';
import 'firebase/database';
import {
	FirebaseDatabaseProvider,
	FirebaseDatabaseNode,
	FirebaseDatabaseTransaction,
} from '@react-firebase/database';
import { useFonts } from '@use-expo/font';
import {
	SkypeIndicator,
} from 'react-native-indicators';
import Icon from 'react-native-vector-icons/FontAwesome';
import { AreaChart } from 'react-native-svg-charts';
import * as shape from 'd3-shape';
import { Defs, LinearGradient as LineGradient, Stop } from 'react-native-svg';
import { bindActionCreators } from 'redux';

import { config } from '../firebase/config';
import { useSelector, useDispatch } from 'react-redux';
import { setPhIndicator, setThumbState } from '../redux/actions/Payload';

const path = 'PH_CONTROLLER/SET_PH';
const pathTanks = 'PH_CONTROLLER/TANKS';
const pathChart = 'PH_CONTROLLER/CHART';

export class Chart extends PureComponent {
	state = {
		filter: 'day'
	}
	render() {
		const data = [5.5, 6.5, 6.8, 7.2];
		const ChartFiltered = () => {
			const { filterReducer: { filter } } = useSelector((state) => state);
			filter === this.state.filter ? null : this.setState({ filter: filter })
			return(
				<FirebaseDatabaseProvider firebase={firebase} {...config}>
					<FirebaseDatabaseNode path={pathChart}>
						{(d) => {
							const { value, isLoading } = d;
							//const getData = !value ? data : data;
							return(
								<AreaChart
									style={{ width: '100%', height: '95%' }}
									data={value ? value[this.state.filter] : data}
									contentInset={{ top: 30, bottom: 30 }}
									curve={shape.curveNatural}
									svg={{ fill: 'url(#gradient)' }}
									animate={true}
								>
									<Gradient/>
								</AreaChart>
							);
						}}
					</FirebaseDatabaseNode>
				</FirebaseDatabaseProvider>
			);
		};

		const Gradient = ({ index }) => (
			<Defs key={index}>
				<LineGradient id={'gradient'} x1={'0%'} y1={'0%'} x2={'50%'} y2={'10%'}>
					<Stop offset={'0%'} stopColor={'#8300FF'} stopOpacity={0.9}/>
					<Stop offset={'100%'} stopColor={'#BE1EED'} stopOpacity={0.8}/>
				</LineGradient>
			</Defs>
		);

		return(
			<ChartFiltered />
		);
	}
}

export const Tanks = ({ tank }) => {
	return (
		<FirebaseDatabaseProvider firebase={firebase} {...config}>
			<FirebaseDatabaseNode path={pathTanks + `/${tank}`}>
				{(d) => {
					const { value, isLoading } = d;
					const name = () => {
						if (!isLoading) {
							if (value > 75) {
								return 'battery-full';
							} else if (value > 50) {
								return 'battery-three-quarters';
							} else if (value > 25) {
								return 'battery-half';
							} else if (value > 0) {
								return 'battery-quarter';
							} else {
								return 'battery-empty';
							}
						}
					};
					!isLoading && value !== null ? console.log(value) : null;
					return !isLoading ? (
						<Icon name={name()} size={20} color="#fff" />
					) : (
						<View></View>
					);
				}}
			</FirebaseDatabaseNode>
		</FirebaseDatabaseProvider>
	);
};

// create a component
export const PhValue = () => {
	const [fontsLoaded] = useFonts({
		Righteous: require('../assets/fonts/Righteous-Regular.ttf'),
	});
	return (
		<FirebaseDatabaseProvider firebase={firebase} {...config}>
			<FirebaseDatabaseNode path={path}>
				{(d) => {
					const { value, isLoading } = d;
					return !isLoading ? (
						<Text
							style={{
								textAlign: 'center',
								color: 'rgba(255,255,255,0.8)',
								fontSize: 72,
								fontWeight: '700',
								fontFamily:
                  					Platform.OS === 'ios'
                  					? fontsLoaded
                  					? 'Righteous'
                  					: 'Futura'
                  					: fontsLoaded
                  					? 'Righteous'
                  					: 'sans-serif-medium',
							}}
						>
							{value} Ph
						</Text>
					) : (
						<View
							style={{
								width: '100%',
								height: '50%',
								display: 'flex',
								justifyContent: 'center',
								alignItems: 'center',
							}}
						>
							<SkypeIndicator color="#fff" />
						</View>
					);
				}}
			</FirebaseDatabaseNode>
		</FirebaseDatabaseProvider>
	);
};

export const PhValueIndicator = () => {
	const { phIndicatorReducer: { phIndicator, thumbState } } = useSelector(state => state);
	const left = Math.round((phIndicator - 5.2) * 100 / 2.3) ;
	return(
		!thumbState ? null : 
		<View style={{
			justifyContent: 'center',
			alignItems: 'center',
			position: 'absolute',
			left: `${left + 7}%`,
			top: 0,
			backgroundColor: '#8300FF',
			borderWidth: 3,
			borderColor: '#bf1fec',
			width: 60,
			height: 60,
			borderRadius: 50,
			zIndex: 1,
			shadowColor: '#000',
			shadowOffset: {
				width: 0,
				height: 9,
			},
			shadowOpacity: 0.48,
			shadowRadius: 11.95,
			
			elevation: 18,
		}}
		>
			<View style={{

				width: 0,
				height: 0,
				bottom: '-40%',
				transform: [{ rotate: '180deg'}],
				borderLeftWidth: 10,
				borderRightWidth: 10,
				borderBottomWidth: 20,
				backgroundColor: 'transparent',
				borderLeftColor: 'transparent',
				borderRightColor: 'transparent',
				borderBottomColor: '#bf1fec',
				position: 'absolute',
				zIndex: 2,
				shadowColor: '#000',
				shadowOffset: {
	 width: 0,
	 height: -6,
				},
				shadowOpacity: 0.48,
				shadowRadius: 11.95,
 
				elevation: 12,
			}}></View>
			<Text style={{ fontSize: 32, color: 'rgba(255,255,255,0.7)' }}>{phIndicator}</Text>
		</View>
	);
};

export const SetPHValue = () => {
	const actions = bindActionCreators({ setPhIndicator, setThumbState }, useDispatch());
	return (
		<FirebaseDatabaseProvider firebase={firebase} {...config}>
			<FirebaseDatabaseTransaction path={path}>
				{({ runTransaction }) => {
					return (
						<FirebaseDatabaseNode path={path}>
							{(d) => {
								const { value, isLoading } = d;
								if (!isLoading && value !== null) {
									return (
										<View
											style={{
												width: '100%',
												height: '50%',
												display: 'flex',
												justifyContent: 'center',
												alignItems: 'center',
											}}
										>
											<LinearGradient
												style={{
													width: '80%',
													display: 'flex',
													justifyContent: 'center',
													alignItems: 'center',
													borderRadius: 22,
												}}
												start={[0, 0.5]}
												end={[1, 0.5]}
												colors={['#bf1fec', '#8300FF']}
											>
												<Slider
													style={style.slider}
													thumbStyle={{
														width: '13%',
														height: '95%',
														borderRadius: 50,
													}}
													thumbTouchSize={{ width: 160, height: 160 }}
													thumbTintColor="#aa09d6"
													value={value * 10}
													step={1}
													maximumValue={68}
													minimumValue={52}
													minimumTrackTintColor={'transparent'}
													maximumTrackTintColor={'transparent'}
													onValueChange={(val) => {
														val % 10 === 0 ? Haptics.selectionAsync() : null;actions.setPhIndicator(val / 10);
													}}
													onSlidingStart={(val) => actions.setThumbState(true)}
													onSlidingComplete={(val) =>
													{runTransaction({
														reducer: () => {
															return val / 10;
														},
													})
														.catch((err) => console.log(err));
													actions.setThumbState(false);
													}
													}
												/>
											</LinearGradient>
										</View>
									);
								} else {
									return <View></View>;
								}
							}}
						</FirebaseDatabaseNode>
					);
				}}
			</FirebaseDatabaseTransaction>
		</FirebaseDatabaseProvider>
	);
};

const style = StyleSheet.create({
	slider: {
		width: '100%',
		backgroundColor: 'transparent',
		borderRadius: 12,
	},
});
