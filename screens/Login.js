//import liraries
import React, { useState } from 'react';
import {
	View,
	Text,
	StyleSheet,
	TouchableOpacity,
	TextInput,
	Platform,
} from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { bindActionCreators } from 'redux';
import { useDispatch, useSelector } from 'react-redux';
import { useFonts } from '@use-expo/font';

import Authenticate, { pwResetEmail } from '../components/Authenticate';
import { setError } from '../redux/actions/Payload';

// create a component
const Login = () => {
	const [fontsLoaded] = useFonts({
		Righteous: require('../assets/fonts/Righteous-Regular.ttf'),
	});
	const actions = bindActionCreators({ setError }, useDispatch());
	const {
		errorReducer: { error },
	} = useSelector((state) => state);
	const [pwEmail, setPwEmail] = useState('');
	const [pwReset, setPwReset] = useState(false);
	return (
		<View style={styles.container}>
			<LinearGradient
				style={{
					width: '100%',
					height: '100%',
					display: 'flex',
					alignItems: 'center',
				}}
				start={[0, 0.5]}
				end={[0, 1]}
				colors={['#8900FD', '#a342e9']}
			>
				<View
					style={{
						width: '100%',
						height: '30%',
						display: 'flex',
						justifyContent: 'center',
						alignItems: 'center',
					}}
				>
					<Text
						style={{
							fontFamily:
                Platform.OS === 'ios'
                	? fontsLoaded
                		? 'Righteous'
                		: 'Futura'
                	: fontsLoaded
                		? 'Righteous'
                		: 'sans-serif-medium',
							fontSize: 48,
							textAlign: 'center',
							color: 'rgba(255,255,255,0.7)',
							fontWeight: '600',
						}}
					>
            Ph Controller
					</Text>
					{error !== null ? (
						<View
							style={{
								width: '90%',
								paddingTop: '20%',
								height: '25%',
								position: 'absolute',
								display: 'flex',
								alignItems: 'center',
								justifyContent: 'center',
							}}
						>
							<Text
								style={{
									fontSize: 21,
									textAlign: 'center',
									color: 'rgba(255,225,225,0.7)',
									fontWeight: '600',
								}}
							>
								{error}
							</Text>
						</View>
					) : (
						<></>
					)}
				</View>
				{!pwReset ? (
					<Authenticate />
				) : (
					<View
						style={{
							width: '80%',
							height: '40%',
							backgroundColor: '#AC5CE5',
							borderRadius: 25,
							display: 'flex',
							justifyContent: 'center',
							alignItems: 'center',
							shadowColor: '#000',
							shadowOffset: {
								width: 0,
								height: 4,
							},
							shadowOpacity: 0.34,
							shadowRadius: 6.3,
							elevation: 12,
						}}
					>
						<View style={{ height: '20%', width: '95%' }}>
							<Text
								style={{
									fontSize: 24,
									textAlign: 'center',
									fontWeight: '700',
									color: 'rgba(255,255,255,0.6)',
								}}
							>
                Password reset e-mail
							</Text>
						</View>
						<View style={styles.input}>
							<TextInput
								onFocus={() => actions.setError(null)}
								autoCapitalize="none"
								value={pwEmail}
								style={{
									width: '100%',
									height: '100%',
									textAlign: 'center',
									color: '#fff',
									fontSize: 22,
								}}
								placeholderTextColor='rgba(255,255,255,0.6)'
								placeholder="Your e-mail address"
								onChange={(val) => {
									setPwEmail(val.nativeEvent.text);
								}}
							/>
						</View>
						<View
							style={{
								width: '55%',
								height: '20%',
								display: 'flex',
								justifyContent: 'center',
								backgroundColor: '#8300A7',
								borderRadius: 32,
							}}
						>
							<TouchableOpacity
								onPress={() => {
									pwResetEmail(pwEmail, actions.setError);
								}}
							>
								<Text
									style={{
										textAlign: 'center',
										fontFamily:
                      Platform.OS === 'ios'
                      	? fontsLoaded
                      		? 'Righteous'
                      		: 'Futura'
                      	: fontsLoaded
                      		? 'Righteous'
                      		: 'sans-serif-medium',
										fontSize: 32,
										fontWeight: '600',
										color: '#fff',
									}}
								>
                  Send
								</Text>
							</TouchableOpacity>
						</View>
					</View>
				)}
				<View style={{ position: 'absolute', bottom: '2.5%' }}>
					<TouchableOpacity
						onPress={() => {
							setPwReset(!pwReset);
							actions.setError(null);
						}}
					>
						<Text
							style={{ fontSize: 21, textAlign: 'center', color: '#740094' }}
						>
							{!pwReset ? 'Forgot your password?' : 'Sign in to Account'}
						</Text>
					</TouchableOpacity>
				</View>
			</LinearGradient>
		</View>
	);
};

// define your styles
const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#2c3e50',
	},
	input: {
		width: '85%',
		height: '20%',
		display: 'flex',
		alignItems: 'center',
		marginBottom: '10%',
		borderRadius: 25,
		backgroundColor: '#AC5CE5',
		borderColor: 'rgba(255,255,255, 0.2)',
		borderWidth: 2,
	},
});

//make this component available to the app
export default Login;
