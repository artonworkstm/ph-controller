//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import DataInput from '../components/DataInput';
import DataVisualizer from '../components/DataVisualizer';

// create a component
const Home = () => {
  return (
    <View style={styles.container}>
      <View style={styles.top}><DataInput /></View>
      <View style={styles.bottom}><DataVisualizer /></View>
    </View>
  );
};

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#2c3e50',
  },
  top: {
    width: '100%',
    height: '45%',
  },
  bottom: {
    width: '100%',
    height: '60%',
    backgroundColor: '#fff'
  }
});

//make this component available to the app
export default Home;
