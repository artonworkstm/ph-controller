import { SET_ERROR, SET_FILTER, SET_PHINDEX, SET_THUMBSTATE } from './Types';

export const setError = (val) => ({
	type: SET_ERROR,
	payload: val,
});

export const setFilter = (val) => ({
	type: SET_FILTER,
	payload: val,
});

export const setPhIndicator = (val) => ({
	type: SET_PHINDEX,
	payload: val,
});

export const setThumbState = (val) => ({
	type: SET_THUMBSTATE,
	payload: val,
});