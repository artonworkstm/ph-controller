import { SET_FILTER } from '../actions/Types';

const initialState = {
  filter: 'day',
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_FILTER:
      return {
        ...state,
        filter: action.payload,
      };
    default:
      return state;
  }
};
