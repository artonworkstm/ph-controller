import { SET_PHINDEX, SET_THUMBSTATE } from '../actions/Types';

const initialState = {
	phIndicator: 0,
	thumbState: false,
};

export default (state = initialState, action) => {
	switch (action.type) {
	case SET_PHINDEX:
		return {
			...state,
			phIndicator: action.payload,
		};
	case SET_THUMBSTATE:
		return {
			...state,
			thumbState: action.payload,
		};
	default:
		return state;
	}
};
