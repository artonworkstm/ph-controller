import { combineReducers } from 'redux';
import errorReducer from './ErrorReducer';
import filterReducer from './FilterReducer';
import phIndicatorReducer from './PhIndicatorReducer';

const rootReducer = combineReducers({
	errorReducer,
	filterReducer,
	phIndicatorReducer,
});

export default rootReducer;
