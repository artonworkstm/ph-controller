import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';
import * as firebase from 'firebase/app';
import 'firebase/auth';
import {
  FirebaseAuthConsumer,
  FirebaseAuthProvider,
} from '@react-firebase/auth';
import { config } from './firebase/config';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Provider } from 'react-redux';

import Home from './screens/Home';
import Login from './screens/Login';
import store from './redux/Store';

export default function App() {
  return (
    <Provider store={store}>
      <KeyboardAwareScrollView
        style={{ backgroundColor: '#4c69a5' }}
        resetScrollToCoords={{ x: 0, y: 0 }}
        contentContainerStyle={styles.container}
        scrollEnabled={false}
      >
        <FirebaseAuthProvider {...config} firebase={firebase}>
          <FirebaseAuthConsumer>
            {({ isSignedIn, user }) => {
              return isSignedIn ? <Home /> : <Login />;
            }}
          </FirebaseAuthConsumer>
        </FirebaseAuthProvider>
      </KeyboardAwareScrollView>
    </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
